#######################################
# Makefile for animalitos_ascii v1
#######################################

#######################################
# Variables
#######################################

# C compiler
CC=gcc

# Files
BASEDIR=.
INCLDIR=$(BASEDIR)/incl
SRCDIR=$(BASEDIR)/src
BINDIR=$(BASEDIR)/bin
DOCDIR=$(BASEDIR)/doc

OUTFILE=animalitos_ascii

# Compiler options and flags
CFLAGS= -std=c11 -g -I $(INCLDIR) -o $(BINDIR)/$(OUTFILE)
LFLAGS=


#######################################
# Targets
#######################################

all: clean build run clean

build: doc
	@echo "w0la"
	@echo "voy a compilar..."
	$(CC) $(CFLAGS) $(LFLAGS) main.c $(SRCDIR)/*.c
	@echo "listo"
	@echo "(uso una @ al inicio del comando para que no salga el comando que ejecuta, solo el resultado)"

clean:
	rm -rf $(BINDIR)/*
	rm -rf $(DOCDIR)/html $(DOCDIR)/latex 

run:
	$(BINDIR)/$(OUTFILE) pato 3

doc:
	doxygen $(DOCDIR)/Doxyfile

.PHONY: all clean doc














