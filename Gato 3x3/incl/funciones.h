/**
 * Gato 3D
 * 2018, nov 2
 * Pamela Salazar E- salazarpamela3@gmail.com
 * License WTFPL4.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h> //strcmp
#include <stdbool.h>

//code guards
#ifndef FUNCIONES_H 
#define FUNCIONES_H

void loop (char c [3][3], int turno);
void create_board(char c [3][3]);
void draw_board(char c [3][3]);
void player1(char c [3][3],int turno);
void player2(char c [3][3],int turno);
int  winner(char c [3][3],int turno);

#endif
