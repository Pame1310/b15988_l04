/**
 * Gato 3D
 * 2018, nov 2
 * Pamela Salazar E- salazarpamela3@gmail.com
 * License WTFPL4.
 **/

#include "funciones.h"

void loop(char c [3][3], int turno){
     int i;
     create_board(c);
     do{
        system("clear");
        draw_board(c);
        if(turno % 2 == 0){
           player1(c,turno);
        }  
        else{
         player2(c,turno);
       }
       turno++;     
       i=winner(c,turno);
     }while(turno <= 9 && i!=0);
  
}

void create_board(char c [3][3]){
     int i, j;
     char aux= 1;
     for (i = 0; i < 3; i++){
	 for(j = 0; j < 3; j++){
	     c[i][j]=aux++;
	 }
     }
       
}



void draw_board(char c [3][3]){
     int i, j;
     for (i = 0; i < 3; i++){
         for(j = 0; j < 3; j++){
             if (j < 2){
                if(c[i][j] != 'x' && c[i][j] != 'o' ){
                   printf(" %d |", c[i][j]);
                }
                else{
                   printf(" %c |", c[i][j]);
                } 
             }
             else{
                 if(c[i][j] != 'x' && c[i][j] != 'o' ){
                   printf(" %d ", c[i][j]);
                }
                else{
                   printf(" %c ", c[i][j]);
                } 
             }
         }
        if (i < 2){
           printf("\n-----------\n");
        }
     }
     printf ("\n");  
}


void player1 (char c [3][3], int turno){
     int  aux,i,j,k;
     char* ptr;
     char* position;
     k=0;
     if(turno!=9){
     do{
    	 do{
       	    printf ("Jugador 1 coloca una ficha: ");       
            scanf ("%d", &aux);
            printf ("\n");
         }while(aux < 1 || aux >  9);
    
     
          for (i = 0; i < 3; i++){
               for(j = 0; j < 3; j++){
                   ptr = &c[i][j];
                   if(*ptr == aux){
                   position = ptr;
                   }
                }
            }
            if(*position != 'x' && *position != 'o'){
   	        *position='x';
                 k=1;      
            }
    }while(k != 1 );
    }
}


void player2 (char c [3][3], int turno){
     int  aux,i,j,k;
     char* ptr;
     char* position;
     k=0;
     if(turno!=9){
     do{
        do{
           printf ("Jugador 2 coloca una ficha: ");        
           scanf ("%d", &aux);
           printf ("\n");
        }while(aux < 1 || aux >  9);
    
        for (i = 0; i < 3; i++){
            for(j = 0; j < 3; j++){
                ptr = &c[i][j];
                if(*ptr == aux){
                position = ptr;
                }
             }
        }
        if(*position != 'x' && *position != 'o'){
           *position='o';
           k=1;      
        }
      }while(k!=1); 
    }
 
}


int winner(char c[3][3],int turno){
     int i, j;
     int k=0;
     for(i = 0; i < 3; i++){
        if( c[i][0]==c[i][1] && c[i][1]==c[i][2]){
           k++;
        }
     }
     for(i = 0; i < 3; i++){
        if( c[0][i]==c[1][i] && c[1][i]==c[2][i]){
           k++;
        }
     }
     
     if ((c[0][0] == c[1][1] && c[1][1] == c[2][2]) || (c[0][2] == c[1][1] && c[1][1] == c[2][0])){
         k++;
     } 
     

     if(k>0){
        system("clear");
        if(turno % 2 == 0){
          printf("Jugador 2 ganaste\n");
        }  
        else{
      	  printf("Jugador 1 ganaste\n");
        }
        return 0;
     }
     else{
       return 1;
     }
}

