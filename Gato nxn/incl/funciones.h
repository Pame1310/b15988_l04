/**
 * Gato 3D
 * 2018, nov 2
 * Pamela Salazar E- salazarpamela3@gmail.com
 * License WTFPL4.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h> //strcmp
#include <stdbool.h>

//code guards
#ifndef FUNCIONES_H 
#define FUNCIONES_H


int n;
void loop (char c [n][n], int turno, int n);
void create_board(char c [n][n]);
void draw_board(char c [n][n], int n);
void player1(char c [n][n],int turno, int n);
void player2(char c [n][n],int turno, int n);
int winner(char c[n][n],int turno,int n);


#endif
