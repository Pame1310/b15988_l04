/**
 * Gato 3D
 * 2018, nov 2
 * Pamela Salazar E- salazarpamela3@gmail.com
 * License WTFPL4.
 **/

#include "funciones.h"

void loop(char c [n][n], int turno, int n){
     int N=n*n;
     int i;
     create_board(c);
     do{
        system("clear");
       
        draw_board(c,n);
        if(turno % 2 == 0){
           player1(c,turno,n);
         }  
         else{
         player2(c,turno,n);
       }
       turno++; 
       i=winner(c,turno,n);      
       }while(turno <=  N  && i != 0);

}

void create_board(char c [n][n]){
     int i, j;
     char aux= 1;
     for (i = 0; i < n; i++){
	 for(j = 0; j < n; j++){
	     c[i][j]=aux++;
	 }
     }
       
}



void draw_board(char c [n][n], int n){
     int i, j;
     int a= n-1;
     for (i = 0; i < n; i++){
         for(j = 0; j < n; j++){
             if (j < a){
                if(c[i][j] != 'x' && c[i][j] != 'o' ){
                   printf(" %d  |", c[i][j]);
                }
                else{
                   printf(" %c  |", c[i][j]);
                } 
             }
             else{
                 if(c[i][j] != 'x' && c[i][j] != 'o' ){
                   printf(" %d  ", c[i][j]);
                }
                else{
                   printf(" %c  ", c[i][j]);
                } 
             }
         }
        if (i < a){
           printf("\n-----------\n");
        }
     }
     printf ("\n");  
}


void player1 (char c [n][n], int turno,int n){
     int  aux,i,j,k,N;
     char* ptr;
     char* position;
     k=0;
     N=n*n;
     if(turno!=N){
       do{
    	 do{
       	    printf ("Jugador 1 coloca una ficha: ");       
            scanf ("%d", &aux);
            printf ("\n");
         }while(aux < 1 || aux >  N);
    
     
          for (i = 0; i < n; i++){
               for(j = 0; j < n; j++){
                   ptr = &c[i][j];
                   if(*ptr == aux){
                     position = ptr;
                   }
                }
            }
            
            if(*position != 'x' && *position != 'o'){
   	       *position='x';
                 k=1;      
            }
  
        }while(k != 1 );
    }
}


void player2 (char c [n][n], int turno,int n){
     int  aux,i,j,k,N;
     char* ptr;
     char* position;
     k=0;
     N=n*n;
     if(turno!=N){
     do{
        do{
           printf ("Jugador 2 coloca una ficha: ");        
           scanf ("%d", &aux);
           printf ("\n");
        }while(aux < 1 || aux >  N);
    
        for (i = 0; i < n; i++){
            for(j = 0; j < n; j++){
                ptr = &c[i][j];
                if(*ptr == aux){
                  position = ptr;
                }
             }
        }
        if(*position != 'x' && *position != 'o'){
           *position='o';
           k=1;      
        }
      }while(k!=1); 
    }
 
}


int winner(char c[n][n],int turno,int n){
     int i=0;
     int j=0;
     int acumf=0;
     int acumc=0;
     int acumd1=0;
     int acumd2=0;
     char aux;

     //Ganador por fila     
     do{
        acumf=0;
       do{
         if(c[i][j]==c[i][j+1]){
            acumf++;
         }
         j++;
       }while(j < n-1);     
       j=0;
       i++;
    } while(i < n && acumf < n-1);
     
    //Ganador Columna
    i=0;
    j=0;
    do{
       acumc=0;
       do{
         if(c[i][j]==c[i+1][j]){
             acumc++;
         }
         i++;
       }while(i < n-1);
       i=0;
       j++;
    }while(j < n && acumc < n-1);

    //Ganador Diagonal 1 
    i=0;
     do{  
         if(c[i][i]==c[i+1][i+1]){
            acumd1++;
         } 
         i++;
    } while(i < n-1  && acumd1 < n-1);
           
     //Ganador Diagonal 2
     i=0; 
     j=n-1;
     do{  
         if(c[i][j]==c[i+1][j-1]){
            acumd2++;
         }
         j=j-1;
         i++;
    } while(i < n-1 && j > 0  && acumd2 < n-1);

         

    //Declarar Ganador     
     if(acumf==n-1 || acumc==n-1 || acumd1==n-1 || acumd2==n-1){
        system("clear");
        if(turno % 2 == 0){
          printf("Jugador 2 ganaste\n");
        }  
        else{
          printf("Jugador 1 ganaste\n");
        }
        return 0;
     }
     else{
       return 1;
     }



}

   


