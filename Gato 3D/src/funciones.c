/**
 * Gato 3D
 * 2018, nov 2
 * Pamela Salazar E- salazarpamela3@gmail.com
 * License WTFPL4.
 **/

#include "funciones.h"

void loop(char c[n][n][n], int turno, int n){
     create_board(c);
     int N=n*n*n;
     int i;
     int j;
     do{
       system("clear");
       draw_board(c,n);
       if (turno < N){
       	  player1(c,turno,n);
       }
       i=winner(c,turno,n);
       if(i==0){
          if(turno % 2 == 0){
             printf("Jugador 1 ganaste \n");
          }
          else{ 
            printf("Jugador 2 ganaste \n");
          }
       }
       turno++;
       j=i;
     }while(turno <= N && j != 0);

}

void create_board(char c [n][n][n]){
     int i,j,k;
     char aux= 1;
     for (k = 0; k < n; k++){
         for(i = 0; i < n; i++){
             for(j = 0; j < n; j++){
                 c[i][j][k]=aux++;
             }        
         }
     }

}


void draw_board(char c [n][n][n], int n){
     int i,j,k;
     int a= n-1;
     for(k=0; k< n; k++){
        printf("\n z = %d\n\n", k);
     	for (i = 0; i < n; i++){
        	 for(j = 0; j < n; j++){
             		if (j < a){
                		if(c[i][j][k] != 'x' && c[i][j][k] != 'o' ){
                  			 printf(" %d  |", c[i][j][k]);
               			 }
				 else{
                  		 	printf(" %c  |", c[i][j][k]);
               			 } 
            		 }
             		 else{
                		 if(c[i][j][k] != 'x' && c[i][j][k] != 'o' ){
                  			 printf(" %d  ", c[i][j][k]);
               			 }
               			 else{
                  			 printf(" %c  ", c[i][j][k]);
                		} 
            		 }
        	 }
       		 if (i < a){
        		 printf("\n-----------\n");
       		 }
	}
        printf ("\n");
     }  
}


void player1 (char c [n][n][n], int turno,int n){
     int  aux,i,j,k,N,final;
     char* ptr;
     char* position;
     k=0;
     final=0;
     N=n*n*n;
     do{
     	do{
        	 if( turno % 2 == 0){
         		printf ("Jugador 1 coloca una ficha: ");       
        	 }
        	 else{
                	printf ("Jugador 2 coloca una ficha: ");
         	 }
         	 scanf ("%d", &aux);
         	 printf ("\n");
    	 }while(aux < 1 || aux >  N);

     	 for (k = 0; k < n; k++){
        	 for(i = 0; i < n; i++){
            		 for(j = 0; j < n; j++){
                		 ptr = &c[i][j][k];
                		 if(*ptr == aux){
                    		 position = ptr;
                		 }
            		 }        
        	 }
    	 }

    	 if(*position != 'x' && *position != 'o'){
       		 if(turno % 2 == 0){
     	  		 *position='x';
       		 } 
       		 else{
          		 *position='o';
        	 } 
                 final=1;
    	}
  
    }while(final != 1);
}

int winner(char c[n][n][n],int turno,int n){
	int i=0;
	int j=0;
	int k=0;
        int aux=0;
	int acumx=0;
        int acumy=0;
        int acumxy=0;
        int acumyx=0;
        int acumz=0;
        int acumzd1=0;
        int acumzd2=0;
        int acumzd3=0;
        int acumzd4=0;
        //Ganador por fila en cualquier plano z
        do{
           do{
              do{
                  if(c[i][j][k]==c[i][j+1][k]){
                     acumx++;
                  } 
                  j++;
              }while(j < n-1);
               if(acumx != n-1){
                    acumx=0;
                  } 
              j=0;
              i++;
           }while(i < n && acumx == 0);
           i=0;
           k++;
        }while(k < n && acumx == 0);
        //Ganador por columna en cualquier plano z
        i=0;
        j=0;
        k=0;       
        do{
           do{
              do{
                  if(c[i][j][k]==c[i+1][j][k]){
                     acumy++;
                  } 
                  i++;
              }while(i < n-1);
              if(acumy != n-1){
                 acumy=0;
              } 
              i=0;
              j++;
           }while(j < n && acumy == 0);
           j=0;
           k++;
        }while(k < n && acumy == 0);
       //Ganador por  diagonal 1  en cualquier plano z
       i=0;
       k=0;
       do{	
           do{
                if(c[i][i][k]==c[i+1][i+1][k]){
                  acumxy++;
                }
                i++;
           }while(i < n);
           if(acumxy != n-1){
             acumxy=0;
           }
           i=0;
           k++;
      }while(k < n && acumxy==0);
     //Ganador por diagonal 2 en cualquier plano z
     i=0;
     j=n-1;
     k=0;
     do{
        do{
           if(c[i][j][k]==c[i+1][j-1][k]){
              acumyx++;
           } 
           i++;
           j=j-1;
        }while(i < n-1 && j > 0);
        if(acumyx != n-1){
           acumyx=0;
        }
        k++;
     }while(k < n);
     //Ganador por columna z
     i=0;
     j=0;
     k=0;
     do{
        i=0;
        do{
           do{
              if(c[i][j][k]==c[i][j][k+1]){
                 acumz++;
              } 
              k++;
           }while(k < n);
           if(acumz != n-1){
             acumz=0;
           }
           i++;
           k=0;
        }while(i < n && acumz==0);       
        j++;
     }while(j < n && acumz==0);
     //Diagonal 1  en z 
     i=0;
     do{
       if(c[i][i][i]==c[i+1][i+1][i+1]){
          acumzd1++;
       }
       i++;
    }while(i < n-1);
    //Diagonal 2 en z
    i=0;
    j=n-1;
    do{
    	if(c[i][j][i]==c[i+1][j-1][i+1]){
          acumzd2++;
        }
        i++;
        j=j-1; 
    }while(i < n-1);
    //Diagonal 3 en z
    i=0;
    j=n-1;
    do{
        if(c[j][i][i]==c[j-1][i+1][i+1]){
            acumzd3++;
        }
        i++;
        j=j-1; 
    }while(i < n-1);
    //Diagonal 4 en z 
    i=0;
    j=n-1;
    do{
        if(c[j][j][i]==c[j-1][j-1][i+1]){
          acumzd4++;
        }
        i++;
        j=j-1; 
    }while(i < n-1);
    
    //Declarar Ganador     
     if(acumx==n-1 || acumy==n-1 || acumxy==n-1 || acumyx==n-1 ||  acumz==n-1 || acumzd1==n-1 || acumzd2==n-1 || acumzd3==n-1 || acumzd4==n-1){
        if(turno % 2 == 0){
        return 0;
        }
        else{
        return 1;
        }
     }

}
